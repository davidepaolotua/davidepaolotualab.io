---
title: What's a game without a player?
layout: page
order: 2
---  
Well, I guess it is pretty easy to find out what we are gonna do this time :)

### Defining the player

Ok, before anything, let's start thinking about what a player is, in the current status of our game.
To put it bluntly, right now a player cannot be anything different from "something that is displayed in the window".

Yes, yes, I know. It's a little bit underwhelming, but fret not. Things are going to change and during this tutorial, it will get some serious power-up!

Now, let's define our beautiful player.

```ruby
class Player
  include Entity

  def initialize( @x  : Int32, @y : Int32, @tile_index : Int32)
  end

end
```

What the hell? It's even less than what I said before! Don't worry, the real stuff is inside `Entity`, but for now, let's say that the player has a position and a tile_index. 

What is the tile index? If you remember from the last tutorial, we generated a tileset from a texture. The tile index just represents which of the various glyphs will be used to draw the player on the map.

However, thinking about the player, it is a particular-but-not-so-particular entity among all the other ones that we will be working with. At the end of our tutorial, we will hopefully have monsters, items, spells, traps, etc... All of these items usually need to be displayed somehow. That's where `entity` comes into play!

`Entity` represents, well, a spatially-defined (i.e: something that has a position) and drawable (something that has a texture) entity.

So, here it is, the `Entity` module in all its glory:
```ruby
require "crsfml"
require "./tilemappeable"

module Entity
  include Tilemappeable

  property x : Int32, y : Int32
  property tile_index : Int32

  # returns the Vertex Array (already textured) representing our entity
  def as_drawable(map_tile_size : Int32) : Array(SF::Vertex)
    texture_coords = Tileset.tile_at_coords(tile_index)
    position_to_tile(x, y, map_tile_size).zip(texture_coords).map { |vertex, texture_vertex|
      SF::Vertex.new(vertex, color: SF.color(100, 250, 50), tex_coords: texture_vertex)
    }
  end

end
```

Ok, ok, ok, nothing too extraordinary, except for the `as_drawable` method. This is in charge of preparing the entity to be drawn on the window. It takes a tile_size (basically the size of each tile in the map), and returns the textured vertexes in the window coordinate system.

Now, let's try to draw something on the map.
Go back to the window class and in the `initialize` method add the following:

```ruby
      @map_tile_size = 50
      @player = Player.new(5, 10, 32)
      @entities = [] of Entity
      @entities << @player
```

And here is our player! Now, if you run the app.. well, nothing happens, as we still need to instruct the window to draw stuff.

So, in the very same class, let's define these two methods:
```ruby
    private def draw_map(window)
      vertex_array = prepare_entities_for_drawing
      window.clear
      states = SF::RenderStates.new
      states.texture = Tileset.texture

      window.draw(vertex_array, states)
      window.display
    end

    private def prepare_entities_for_drawing
      @entities.each_with_object(SF::VertexArray.new(SF::Quads)) do |entity, accumulator|
        entity.as_drawable(@map_tile_size).each do |vertex|
          accumulator.append(vertex)
        end
      end
    end
```
and add the call in the `manage_events` method:
```ruby
    while event = window.poll_event
        if event.is_a? SF::Event::Closed
          window.close
        end
        draw_map(window)
    end
```

So, what is happening? well, simply put, everytime there is a polling for an event, afterwards we draw the map in the window.
To do so, we prepare all our entities as a `SF::VertexAray`, organized to draw the set of vertexes as unconnected quads. Then, we clear the window, we set the texture to use(all our entities for now share the same texture, they just point at different subsquares of it), we draw the `VertexArray` in the back-buffer and we finally display it.

It's time to commit and push everything :) You can find the repo at my gitlab tagged with [1](https://gitlab.com/davidepaolotua/crystal-rogue/-/tree/1-setup-and-displaying-entities)

Now, let's move the player around.

First of all, let's define two new modules, `Moveable` which will take care of all movements, and `Actor` that will be a wrapper for all "sentient" entities:

```ruby
module Moveable

  property x, y

  def move(dx : Int32, dy : Int32)
    @x += dx
    @y += dy
  end

end
```

```ruby
module Actor
  include Entity
  include Moveable

end
```

Now, let's change player to let it become an Actor, instead of a plain Entity.

```diff
-require "./entity"
+require "./actor"

 class Player
-  include Entity
+  include Actor
```

And change the `manage_events` method to respond to keypresses:
```diff
         if event.is_a? SF::Event::Closed
           window.close
         end
+        if event.is_a? SF::Event::KeyPressed
+          delta = case (event.code)
+                  when .a?
+                    [-1, 0]
+                  when .w?
+                    [0, -1]
+                  when .s?
+                    [0, 1]
+                  when .d?
+                    [1, 0]
+                  else
+                    [0,0]
+                  end
+          @player.move(delta[0], delta[1])
+        end
         draw_map(window)
```
Not the nicest code, I agree, but we will change it soon enough.
The code is pretty self-explanatory, but just notice one thing: when you press the `w` key you actually move **down** in the window coordinate system, when you press `s` you instead move **up**. That's because the (0,0) coordinate is actually the top-left of the screen:


Now, if you let the system run, you'll finally be able to move the player. Notice however that there is nothing preventing you from going out of the screen. 
This, and the window code actually sucks right now... So let's refactor stuff!


### Refactoring and cleaning up

First of all, let's not keep all files together in the crystal_rogue folder.
Let's create this folders to organize stuff a little bit better.
- entities: will contain the entity definitions - like, a player, a zombie, a troll, a treasure chest...
  - move player.cr inside here
- behaviors: all the modules that will get included by the entities:
  - move actor.cr, entity.cr, moveable.cr inside
- gui_related: stuff related to gui
  - tilemappeable.cr and tileset.cr can be safely moved here
- actions: will contain the actions definitions. An action is basically a glorified version of an event, and provides a uniform interface to be performed.
- event_handlers: will map the basic events to the relative actions. 

Obviously, we don't have yet the files to fill the last two folders.. Let's create them.
Create a base definition for action.cr
```ruby
module Action
  abstract def perform
end
```
Nothing too complicated, we just define our base contract that our "real" actions will abide to.
We will need right now three actions:
- the action to move our player around
- the action to exit the game
- an empty action, in case for example one presses the key `q` instead of `w`

```ruby
require "crsfml"
require "./action"

record QuitAction, window : SF::Window do
  include Action

  def perform
    window.close
  end
end
```

```ruby
require "./action"

record EmptyAction do
  include Action

  def perform
  end
end
```

```ruby
require "./action"
require "../behaviors/moveable"

enum Directions
  UP
  DOWN
  LEFT
  RIGHT
end

record MoveAction, target : Moveable, direction : Directions do
  include Action

  def perform
    delta = case (direction)
            when Directions::UP
              [0, -1]
            when Directions::DOWN
              [0, 1]
            when Directions::LEFT
              [-1, 0]
            when Directions::RIGHT
              [1, 0]
            else
              raise "Something bizarre happened, #{direction}"
            end
    target.move(delta[0], delta[1])
  end
end
```

Now for the event handlers. Basically, we want the event handler to be as flexible as possible - as we may want to be able to swap them on the fly if needed (maybe in the normal map view the key `c` makes you sleep, while in the inventory menu it may select the consume option)
So, we will have a base event handler which takes care of the extremely basic stuff (e.g: you quit the game), and another one that right now takes care of only the movement - but will be improved soon :)

```ruby
require "../actions/**"

class BaseEventHandler
  def parse_and_dispatch(event, window, game)
    action = parse_event(event, window, game)
    dispatch_event(action)
  end

  def parse_event(event, window, game)
    if event.is_a? SF::Event::Closed
      QuitAction.new(window)
    else
      EmptyAction.new
    end
  end

  def dispatch_event(action : Action)
    action.perform
  end
end
```

```ruby
require "./base_event_handler"
require "../actions/**"

class EventHandler < BaseEventHandler
  def parse_event(event, window, game)
    if event.is_a? SF::Event::KeyPressed
      case (event.code)
      when .a?
        MoveAction.new(game.player, Directions::LEFT)
      when .w?
        MoveAction.new(game.player, Directions::UP)
      when .s?
        MoveAction.new(game.player, Directions::DOWN)
      when .d?
        MoveAction.new(game.player, Directions::RIGHT)
      else
        super(event, window, game)
      end
    else
      super(event, window, game)
    end
  end
end
```

Finally, let's expose the player and the entities in the `Window` class:
```diff
 module CrystalRogue
   class Window
-    @player : Player
-    @entities : Array(Entity)
+    getter player : Player
+    getter entities : Array(Entity)

  ...
       @entities = [] of Entity
       @entities << @player
+      @event_handler = EventHandler.new
     end

  ...   
       while event = window.poll_event
-        if event.is_a? SF::Event::Closed
-          window.close
-        end
-        if event.is_a? SF::Event::KeyPressed
-          delta = case (event.code)
-                  when .a?
-                    ...
-                  end
-          @player.move(delta[0], delta[1])
-        end
+        @event_handler.parse_and_dispatch(event, window, self)
         draw_map(window)
```

Now, it's a little bit cleaner :)







