---
title: Environment Setup & Basic Window
layout: page
order: 1
---  

This post is the first of a (hopefully long) series in which we will be building a very basic roguelike.
In particular, I will be using Crystal (www.crystal-lang.org) as the base programming language, coupled with CrSfml to manage the graphical interface.

So, let's get started!

First thing we want to do is to create the basic app, so let's fire up the terminal and run:
```
crystal init app crystal_rogue
cd crystal_rogue
```
This will setup the base skeleton of an app, initialize the repo, so that we can move on.

### Adding the GUI library
First of all, install SFML (Simple and Fast Multimedia Library). Instructions on how to do so are different depending on the operating system - so I will let you do that. In case of MacOs, it was a simple `brew install sfml`.
Now, to be able to use it in our game, we will need the bindings for `sfml`, so let's open the shards.yml and add them.

```ruby
  crsfml:
    github: oprypin/crsfml
    version: ~> 2.5.2
```    
Followed by a `shards install`

### Creating our first window
Now, let's test that everything is setup correctly. Open the file in `src/crystal_rogue.cr` and add the following inside the module declaration, to start the application:

```ruby
  Window.new("Crystal Rogue", 800, 600).run
```

Now, this obviously won't even compile, as we haven't defined the new Window class yet, so let's do it.
Create a new folder in `src` called `crystal_rogue`, require it inside the main file and then create a new filoe `window.cr` with the following content:

```ruby
require "crsfml"

module CrystalRogue
  class Window
    def initialize(@name : String, @screen_width : Int32, @screen_height : Int32)
    end

    def run
      window = SF::RenderWindow.new(SF::VideoMode.new(@screen_width, @screen_height), @name)
    end
  end
end
```

Nice, but... now it compiles, but if you were expecting to see anything, you're in for a nasty surprise.
It seems that nothing is happening, but that's because we are actually missing the event loop. So let's add it!
In the window class change the `run()` method in this way:

```ruby
    def run
      window = SF::RenderWindow.new(SF::VideoMode.new(@screen_width, @screen_height), @name)
      while window.open?
        manage_events(window)
      end
    end

    private def manage_events(window)
      while event = window.poll_event
        if event.is_a? SF::Event::Closed
          window.close
        end
      end
    end
```
Basically, here we're setting up what is called the event loop. While the window is open, it waits for events, then it will do something according to our needs. For now, we are just interested in having a way to close the window, so the only event we care about is the Closed one.
Try to run it, and you will now see that the window is (at last!) created and waiting for your input - in this case, to be closed.

### Preparing to draw
Before calling it a day, let's do one last thing, let us prepare for what will come next.
We will need to show something in the window at some point, so let's create something that will load some textures.
All sprites that we will need to draw will be taken from this image:

![Tilesets](/assets/arial10x10.png)

Let's start by defining a small utility module `Tilemappeable`, which for now will contain only a single method:
```ruby
# This module gives the possibility of translating from map coords to a tilemap coord.
# So, if the tile_size is 10 and the position is (3,1) it will return a square of size 10 whose topleft corner is at (30,10)
module Tilemappeable
  # Given a position in form x,y returns a square of tile_size size
  def position_to_tile(x, y, tile_size) : Array(Tuple(Int32, Int32))
    [{x, y}, {x, y + 1}, {x + 1, y + 1}, {x + 1, y}].map { |tuple| {tuple[0]*tile_size, tuple[1] * tile_size} }
  end
end
```
It is quite self-explanatory, but in rough terms it gets a (x,y) coord and returns the square represented by that coord.

Now, let's put the image above to good use. We will load it whole as a texture, and provide some methods to access any subpart of it by index. So, for example, if we wanted to obtain the `!` character we would ask for tile at index equal to 1, if we wanted `@` instead, the index is 33, etc...

```ruby
require "crsfml"
require "./tilemappeable"

module Tileset
  extend self
  include Tilemappeable

  @@texture = SF::Texture.from_file("assets/arial10x10.png")

  def texture
    @@texture
  end

  def tile_at_coords(index : Int32) : Array(Tuple(Int32, Int32))
    row_index = index // number_of_tiles_per_row
    column_index = index % number_of_tiles_per_row
    position_to_tile(column_index, row_index, tile_size)
  end

  private def number_of_tiles_per_row
    32
  end

  private def tile_size
    10
  end
end
```
For now, it is a singleton object, but nothing will prevent us to extend it later and have multiple tilesets all together.





