---
layout: page
title: Blog
permalink: /blog/
---

Here you can find something I wanted to write.

<h1>Roguelike in Crystal Tutorial</h1>
<ol>
{% for roguelike_post in site.roguelike_posts%}
  <li>
    <a href="{{ roguelike_post.url }}">{{ roguelike_post.title }}</a>
  </li>
{% endfor %}
</ol>